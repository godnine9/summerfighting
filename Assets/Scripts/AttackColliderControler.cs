﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackColliderControler : MonoBehaviour {

	public PhotonPlayer mPhotonPlayer;

	private void OnTriggerEnter(Collider iTarget)
	{
		Debug.Log(iTarget.gameObject.name);
		PhotonPlayer iTargetPlayer = iTarget.GetComponent<PhotonPlayer>(); 
		if(iTargetPlayer != null)
		{
			if(iTargetPlayer.MyPlayerInfo.Name != mPhotonPlayer.MyPlayerInfo.Name)
			{
				mPhotonPlayer.OnHit(iTargetPlayer);
			}
		}
	}
}
