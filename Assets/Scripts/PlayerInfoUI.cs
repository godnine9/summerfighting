﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoUI : MonoBehaviour
{
    public Text NickName;
    public Text Ready;
    public Image Icon;
    public GameObject Master;

    [PunRPC]
    public void SetNickname (string iName)
    {
        NickName.text = iName;
    }

    [PunRPC]
    public void SetReady(bool iReady)
    {
        if(iReady)
        {
            Ready.text = "Ready";
            Ready.color = Color.green;
        }
        else
        {
            Ready.text = "Preparing";
            Ready.color = Color.red;
        }
        
    }

    [PunRPC]
    public void SetMaster(bool iMaster)
    {
        Master.SetActive(iMaster);
    }
}
